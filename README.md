# PS4 Camera Driver for Linux

Supports both `CUH-ZEY1` and `CUH-ZEY2` models.

**Currently, the driver only flashes the required firmware** and reboots the camera.
After that, the camera will be picked up by the generic `uvcvideo` driver, and you will have a weird video output.
This output can be cropped in [OBS](https://obsproject.com/), and then streamed through a virtual camera.

This repository doesn't provide the necessary firmware due to unknown legality.
You will have to search for it yourself, download it, and `mv ~/Downloads/firmware.bin /lib/firmware/ps4cam.bin`.

To install this driver you will need `make`, `gcc` and `kernel-headers` or `linux-headers` installed.
Then copy the repository and run the following commands:
```
make                    # To build
insmod ps4cam.ko        # To install
```

To remove the driver, run the following command:
```
rmmod ps4cam    # To remove
```
