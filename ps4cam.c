/* SPDX-License-Identifier: GPL-2.0-only */
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/usb.h>
#include <linux/firmware.h>

MODULE_DESCRIPTION("PlayStation 4 Camera driver");
MODULE_AUTHOR("Maksym Hazevych <mhazevych@proton.me>");
MODULE_LICENSE("GPL v2");

#define USB_CAM_VENDOR_ID	0x05a9
#define USB_CAM_PRODUCT_ID	0x0580
#define FIRMWARE_CAM_NAME	"ps4cam.bin"
#define CHUNK_SIZE		512

static const struct usb_device_id cam_table[] = {
	{USB_DEVICE(USB_CAM_VENDOR_ID, USB_CAM_PRODUCT_ID)},
	{}
};

MODULE_DEVICE_TABLE(usb, cam_table);

struct usb_cam {
	struct usb_device	*udev;
	struct usb_interface	*interface;
};

static int flash_firmware(struct usb_cam *dev, unsigned int pipe,
		const struct firmware *fw)
{
	size_t value;
	size_t size;

	for (value = 0; value < fw->size; value += size) {
		size = min_t(size_t, CHUNK_SIZE, fw->size - value);

		usb_control_msg_send(dev->udev, pipe, 0x0, 0x40, value, 0x14,
			fw->data + value, size, USB_CTRL_SET_TIMEOUT, GFP_KERNEL);
	}

	if (value != fw->size) {
		dev_err(&dev->interface->dev, 
			"Error sending firmware, firmware size %ld, bytes sent %ld",
			fw->size, value);
		return -EIO;
	}

	return 0;
}

static int cam_probe(struct usb_interface *interface,
		const struct usb_device_id *id)
{
	struct usb_cam *dev;
	int retval = 0;
	unsigned int pipe;
	const struct firmware *fw;
	const char reload_value[] = {0x5b};

	dev = devm_kzalloc(&interface->dev, sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		retval = -ENOMEM;
		goto error;
	}

	dev->udev = usb_get_dev(interface_to_usbdev(interface));
	dev->interface = usb_get_intf(interface);

	usb_set_intfdata(interface, dev);

	if (request_firmware(&fw, FIRMWARE_CAM_NAME, &dev->udev->dev)) {
		dev_err(&interface->dev, "No firmware found");
		retval = -ENOENT;
		goto error;
	}

	pipe = usb_sndctrlpipe(dev->udev, 0);

	retval = flash_firmware(dev, pipe, fw);
	if (retval)
		goto ctrl_error;

	dev_info(&interface->dev, "Firmware flashed");

	/* 
	 * This message reloads the camera. But it doesn't complete the
	 * message, therefore we don't wait for it (using 1ms timeout).
	 */
	usb_control_msg_send(dev->udev, pipe, 0x0, 0x40, 0x2200, 0x8018,
			reload_value, 1, 1, GFP_KERNEL);

ctrl_error:
	release_firmware(fw);
error:
	return retval;
}

static void cam_disconnect(struct usb_interface *interface)
{
	struct usb_cam *dev;

	dev = usb_get_intfdata(interface);
	usb_put_dev(dev->udev);
	usb_put_intf(interface);
}

static struct usb_driver cam_driver = {
	.name		= "ps4cam",
	.probe		= cam_probe,
	.disconnect	= cam_disconnect,
	.id_table	= cam_table,
};

module_usb_driver(cam_driver);
